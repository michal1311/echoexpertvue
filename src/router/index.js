import Vue from "vue";
import Router from "vue-router";
import Hello from "@/components/Hello";
import Navigation from "@/components/Navigation";
import Myfooter from "@/components/Myfooter";
import Information from "@/components/Information";
import Echo2025 from "@/components/Echo2025";
import Echo2024 from "@/components/Echo2024";
import Echo2023 from "@/components/Echo2023";
import Echo2022 from "@/components/Echo2022";
import Echo2021 from "@/components/Echo2021";
import Echo2020 from "@/components/Echo2020";
import Echo2019 from "@/components/Echo2019";
import Echo2018 from "@/components/Echo2018";
import Echo2017 from "@/components/Echo2017";
import Echo2016 from "@/components/Echo2016";
import Echo2015 from "@/components/Echo2015";
import Echo2014 from "@/components/Echo2014";
import Echo2013 from "@/components/Echo2013";
import Echo2012 from "@/components/Echo2012";
import Echo2011 from "@/components/Echo2011";
import Echo2010 from "@/components/Echo2010";
import Galery2016 from "@/components/Galery2016";
import Galery2015 from "@/components/Galery2015";
import Galery2014 from "@/components/Galery2014";
import Galery2013 from "@/components/Galery2013";
import Galery2012 from "@/components/Galery2012";
import GalleryEE2019 from "@/components/GalleryEE2019";
import GalleryEE2020 from "@/components/GalleryEE2020";
import GalleryEE2019Warszawa from "@/components/GalleryEE2019Warszawa";
import GalleryEE2019Opinogora from "@/components/GalleryEE2019Opinogora";
import GalleryEE2019Bydgoszcz from "@/components/GalleryEE2019Bydgoszcz";
import GalleryEE2020online from "@/components/GalleryEE2020online";
import GalleryEE2021online from "@/components/GalleryEE2021online";
require("jquery/dist/jquery.js");
require("bootstrap/dist/css/bootstrap.min.css");

Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      name: "Hello",
      component: Echo2025,
    },
    {
      path: "/",
      name: "Navigation",
      component: Navigation,
    },
    {
      path: "/",
      name: "Myfooter",
      component: Myfooter,
    },
    {
      path: "/information",
      name: "Information",
      component: Information,
    },
    {
      path: "/echo2024",
      name: "Echo2024",
      component: Echo2024,
    },
    {
      path: "/echo2025",
      name: "Echo2025",
      component: Echo2025,
    },
    {
      path: "/echo2023",
      name: "Echo2023",
      component: Echo2023,
    },
    {
      path: "/echo2022",
      name: "Echo2022",
      component: Echo2022,
    },
    {
      path: "/echo2021",
      name: "Echo2021",
      component: Echo2021,
    },
    {
      path: "/echo2020",
      name: "Echo2020",
      component: Echo2020,
    },
    {
      path: "/echo2019",
      name: "Echo2019",
      component: Echo2019,
    },
    {
      path: "/echo2018",
      name: "Echo2018",
      component: Echo2018,
    },
    {
      path: "/echo2017",
      name: "Echo2017",
      component: Echo2017,
    },
    {
      path: "/echo2016",
      name: "Echo2016",
      component: Echo2016,
    },
    {
      path: "/echo2015",
      name: "Echo2015",
      component: Echo2015,
    },
    {
      path: "/echo2014",
      name: "Echo2014",
      component: Echo2014,
    },
    {
      path: "/echo2013",
      name: "Echo2013",
      component: Echo2013,
    },
    {
      path: "/echo2012",
      name: "Echo2012",
      component: Echo2012,
    },
    {
      path: "/echo2011",
      name: "Echo2011",
      component: Echo2011,
    },
    {
      path: "/echo2010",
      name: "Echo2010",
      component: Echo2010,
    },
    {
      path: "/galery2016",
      name: "Galery2016",
      component: Galery2016,
    },
    {
      path: "/galery2015",
      name: "Galery2015",
      component: Galery2015,
    },
    {
      path: "/galery2014",
      name: "Galery2014",
      component: Galery2014,
    },
    {
      path: "/galery2013",
      name: "Galery2013",
      component: Galery2013,
    },
    {
      path: "/galery2012",
      name: "Galery2012",
      component: Galery2012,
    },
    {
      path: "/gallery2019",
      name: "Gallery2019",
      component: GalleryEE2019,
    },
    {
      path: "/gallery2020",
      name: "Gallery2020",
      component: GalleryEE2020,
    },
    {
      path: "/Gallery-Warszawa2019",
      name: "Gallery-Warszawa2019",
      component: GalleryEE2019Warszawa,
    },
    {
      path: "/Gallery-Opinogora2019",
      name: "Gallery-Opinogora2019",
      component: GalleryEE2019Opinogora,
    },
    {
      path: "/Gallery-Bydgoszcz2019",
      name: "Gallery-Bydgoszcz2019",
      component: GalleryEE2019Bydgoszcz,
    },
    {
      path: "/Gallery-2020-online",
      name: "Gallery-2020-online",
      component: GalleryEE2020online,
    },
    {
      path: "/Gallery-2021-online",
      name: "Gallery-2021-online",
      component: GalleryEE2021online,
    },
  ],
  scrollBehavior(to, from, savedPosition) {
    return { x: 0, y: 0 };
  },
});
